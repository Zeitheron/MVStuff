package com.zeitheron.mvstuff;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.mvstuff.commands.CommandMV;
import com.zeitheron.mvstuff.init.EntitiesMV;
import com.zeitheron.mvstuff.init.GuiCallbacksMV;
import com.zeitheron.mvstuff.init.ItemsMV;
import com.zeitheron.mvstuff.init.PotionsMV;
import com.zeitheron.mvstuff.items.ItemSkilledFish;
import com.zeitheron.mvstuff.proxy.ServerProxy;
import com.zeitheron.mvstuff.utils.Cheaters.CommandMVGW;
import com.zeitheron.mvstuff.utils.loot.LootConditionSkilledFish;
import com.zeitheron.mvstuff.utils.loot.LootEntryItemStack;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.world.storage.loot.LootEntry;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.world.storage.loot.RandomValueRange;
import net.minecraft.world.storage.loot.conditions.LootCondition;
import net.minecraft.world.storage.loot.functions.LootFunction;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod(modid = InfoMV.MOD_ID, name = InfoMV.MOD_NAME, version = InfoMV.VERSION, certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", dependencies = "required-after:hammercore;required-after:baubles", updateJSON = "https://pastebin.com/raw/KMLPHtBJ")
public class MVStuff
{
	public static final Logger LOG = LogManager.getLogger(InfoMV.MOD_ID);
	
	@SidedProxy(clientSide = InfoMV.CLIENT_PROXY, serverSide = InfoMV.SERVER_PROXY)
	public static ServerProxy proxy;
	
	public static final CreativeTabs tab = new CreativeTabs(InfoMV.MOD_ID)
	{
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(ItemsMV.BALAGUR_COIN);
		}
	};
	
	public static final CreativeTabs tabAdvancements = new CreativeTabs(InfoMV.MOD_ID + ".adv")
	{
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(ItemsMV.ADV_MINEVOST);
		}
	};
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		PotionsMV.register();
		EntitiesMV.register();
		MinecraftForge.EVENT_BUS.register(this);
		MinecraftForge.EVENT_BUS.register(proxy);
		SimpleRegistration.registerFieldItemsFrom(ItemsMV.class, InfoMV.MOD_ID, tab);
		proxy.preInit();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		GuiCallbacksMV.register();
		proxy.init();
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		e.registerServerCommand(new CommandMVGW());
		e.registerServerCommand(new CommandMV());
	}
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with Minevostorg Stuff jar!");
		LOG.warn("This issue is abnormal and should not happen!");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put("mvstuff", "http://minevost.org");
	}
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void lootLoad(LootTableLoadEvent e)
	{
		if(e.getName().equals(LootTableList.GAMEPLAY_FISHING_FISH))
		{
			Random rand = new Random();
			for(ItemSkilledFish fish : ItemSkilledFish.FISHES)
			{
				LootEntry entry = new LootEntryItemStack(new ItemStack(fish), 100, 200, new LootFunction[0], new LootCondition[0], fish.getRegistryName().toString());
				LootPool pool1 = new LootPool(new LootEntry[] { entry }, new LootCondition[] { new LootConditionSkilledFish(fish) }, new RandomValueRange(1), new RandomValueRange(0, 1), fish.getRegistryName().toString());
				try
				{
					e.getTable().addPool(pool1);
					
					int valids = 0;
					
					int tests = 1000;
					for(int i = 0; i < tests; ++i)
						if(fish.chance.apply(rand))
							++valids;
						
					float chance = valids / (float) tests;
					
					LOG.info("Added fish " + fish.getRegistryName() + " to fishing loot from level " + String.format("%.2f", fish.lvl) + " with average " + String.format("%.2f", chance * 100F) + "% chance!");
				} catch(Throwable err)
				{
					LOG.warn("Failed to add fish " + fish.getRegistryName() + " to fishing loot!");
				}
			}
		}
	}
}