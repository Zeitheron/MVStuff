package com.zeitheron.mvstuff.init;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.client.gui.IGuiCallback;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.mvstuff.inventory.ContainerStorageBelt;
import com.zeitheron.mvstuff.inventory.GuiStorageBelt;
import com.zeitheron.mvstuff.items.ItemStorageBelt;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class GuiCallbacksMV
{
	public static ItemStack getStorageBelt(EntityPlayer player)
	{
		IBaublesItemHandler ibh = BaublesApi.getBaublesHandler(player);
		for(int i : BaubleType.BELT.getValidSlots())
		{
			ItemStack stack = ibh.getStackInSlot(i);
			if(!stack.isEmpty() && stack.getItem() instanceof ItemStorageBelt)
				return stack;
		}
		return ItemStack.EMPTY;
	}
	
	public static final IGuiCallback BELT_CALLBACK = IGuiCallback.create((player, location) ->
	{
		ItemStack belt = getStorageBelt(player);
		return !belt.isEmpty() ? new GuiStorageBelt(player, belt) : null;
	}, (player, location) ->
	{
		ItemStack belt = getStorageBelt(player);
		return !belt.isEmpty() ? new ContainerStorageBelt(player, belt) : null;
	});
	
	public static void register()
	{
		for(Field f : GuiCallbacksMV.class.getDeclaredFields())
		{
			f.setAccessible(true);
			if(IGuiCallback.class.isAssignableFrom(f.getType()))
				try
				{
					GuiManager.registerGuiCallback((IGuiCallback) f.get(null));
				} catch(Throwable err)
				{
				}
		}
	}
}