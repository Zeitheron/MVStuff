package com.zeitheron.mvstuff.init;

import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.entity.EntityOldMonkProjectile;
import com.zeitheron.mvstuff.entity.EntityOverlordProjectile;
import com.zeitheron.mvstuff.entity.EntityZeitheronProjectile;

import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class EntitiesMV
{
	public static final EntityEntry OVERLORD_PROJ = new EntityEntry(EntityOverlordProjectile.class, InfoMV.MOD_ID + ":overlord_projectile").setRegistryName(InfoMV.MOD_ID, "overlord_projectile");
	public static final EntityEntry ZEITHERON_PROJ = new EntityEntry(EntityZeitheronProjectile.class, InfoMV.MOD_ID + ":zeitheron_projectile").setRegistryName(InfoMV.MOD_ID, "zeitheron_projectile");
	public static final EntityEntry OLD_MONK_PROJ = new EntityEntry(EntityOldMonkProjectile.class, InfoMV.MOD_ID + ":old_monk_projectile").setRegistryName(InfoMV.MOD_ID, "old_monk_projectile");
	
	public static void register()
	{
		ForgeRegistries.ENTITIES.register(OVERLORD_PROJ);
		ForgeRegistries.ENTITIES.register(ZEITHERON_PROJ);
		ForgeRegistries.ENTITIES.register(OLD_MONK_PROJ);
	}
}