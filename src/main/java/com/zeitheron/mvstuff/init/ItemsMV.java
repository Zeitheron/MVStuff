package com.zeitheron.mvstuff.init;

import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.MVStuff;
import com.zeitheron.mvstuff.items.ItemAdvancementIcon;
import com.zeitheron.mvstuff.items.ItemOldMonkWand;
import com.zeitheron.mvstuff.items.ItemOverlordWand;
import com.zeitheron.mvstuff.items.ItemProjectileIcon;
import com.zeitheron.mvstuff.items.ItemScrollFood;
import com.zeitheron.mvstuff.items.ItemScrollHealth;
import com.zeitheron.mvstuff.items.ItemScrollNegative;
import com.zeitheron.mvstuff.items.ItemSkilledFish;
import com.zeitheron.mvstuff.items.ItemStorageBelt;
import com.zeitheron.mvstuff.items.ItemUnariusNegators;
import com.zeitheron.mvstuff.items.ItemZeitheronWand;

import net.minecraft.item.Item;

public class ItemsMV
{
	public static final Item GOLD_BAG = new Item().setTranslationKey("gold_bag");
	public static final Item BIG_GOLD_BAG = new Item().setTranslationKey("big_gold_bag");
	public static final Item VEXEL = new Item().setTranslationKey("vexel");
	public static final Item BALAGUR_COIN = new Item().setTranslationKey("balagur_coin");
	public static final Item LEPRECON_POT = new Item().setTranslationKey("leprecon_pot");
	public static final ItemOverlordWand OVERLORD_WAND = new ItemOverlordWand();
	public static final ItemZeitheronWand ZEITHERON_WAND = new ItemZeitheronWand();
	public static final ItemOldMonkWand OLD_MONK_WAND = new ItemOldMonkWand();
	
	public static final ItemScrollNegative //
	SCROLL_ZDMG_I = new ItemScrollNegative(60, 5, 1), //
	        SCROLL_ZDMG_II = new ItemScrollNegative(60, 10, 2), //
	        SCROLL_ZDMG_III = new ItemScrollNegative(60, 15, 3), //
	        SCROLL_ZDMG_IV = new ItemScrollNegative(60, 20, 4);
	
	public static final ItemScrollHealth SCROLL_HEALTH = new ItemScrollHealth(8F, 30);
	public static final ItemScrollFood SCROLL_FOOD = new ItemScrollFood(false);
	public static final ItemScrollFood SCROLL_FOOD_FULL = new ItemScrollFood(true);
	
	public static final ItemStorageBelt BELT_1 = new ItemStorageBelt(1);
	public static final ItemStorageBelt BELT_2 = new ItemStorageBelt(2);
	public static final ItemStorageBelt BELT_3 = new ItemStorageBelt(3);
	
	public static final ItemUnariusNegators UNARIUS_NEGATORS = new ItemUnariusNegators();
	
	public static final Item CYAN_BUG = new Item().setTranslationKey("bug_cyan");
	public static final Item SWAMP_BUG = new Item().setTranslationKey("bug_swamp");
	public static final Item VIONLITHIC_BUG = new Item().setTranslationKey("bug_vionlithic");
	public static final Item BLOOD_BUG = new Item().setTranslationKey("bug_blood");
	public static final Item SILMARINE_BUG = new Item().setTranslationKey("bug_silmarine");
	
	public static final Item BUTTERFLY_QUEEN_NIGHT = new Item().setTranslationKey("butterfly_queen_night");
	public static final Item BUTTERFLY_LAZURE_SMOKER = new Item().setTranslationKey("butterfly_lazure_smoker");
	public static final Item BUTTERFLY_TENDERIA = new Item().setTranslationKey("butterfly_tenderia");
	public static final Item BUTTERFLY_FIERY_FENIX = new Item().setTranslationKey("butterfly_fiery_fenix");
	public static final Item BUTTERFLY_OPAL_NIGHT = new Item().setTranslationKey("butterfly_opal_night");
	
	public static final ItemAdvancementIcon //
	ADV_TRAVEL_TRIP = new ItemAdvancementIcon("travel_trip"), //
	        ADV_MINEVOST = new ItemAdvancementIcon("minevost"), //
	        ADV_FISH_1 = new ItemAdvancementIcon("fish_1"), //
	        ADV_FISH_10 = new ItemAdvancementIcon("fish_10"), //
	        ADV_FISH_50 = new ItemAdvancementIcon("fish_50"), //
	        ADV_FISH_100 = new ItemAdvancementIcon("fish_100"), //
	        ADV_FISH_500 = new ItemAdvancementIcon("fish_500"), //
	        ADV_FLVL_1 = new ItemAdvancementIcon("flvl_1"), //
	        ADV_FLVL_2 = new ItemAdvancementIcon("flvl_2"), //
	        ADV_FLVL_3 = new ItemAdvancementIcon("flvl_3"), //
	        ADV_FLVL_4 = new ItemAdvancementIcon("flvl_4"), //
	        ADV_FLVL_5 = new ItemAdvancementIcon("flvl_5");
	
	public static final ItemProjectileIcon //
	PROJECTILE_OVERLORD = new ItemProjectileIcon("overlord"), //
	        PROJECTILE_ZEITHERON = new ItemProjectileIcon("zeitheron"), //
	        PROJECTILE_OLD_MONK = new ItemProjectileIcon("old_monk");
	
	public static final TwoTuple<ItemSkilledFish, ItemAdvancementIcon[]> //
	BARBALON = generateFish("barbalon", 0, true), //
	        VESNYAK = generateFish("vesnyak", 0, true), //
	        DEEPWATER_WILLY = generateFish("deepwater_willy", 0, true), //
	        GUBAN = generateFish("guban", 0, true), //
	        DINICHTHYS = generateFish("dinichthys", 0, true), //
	        ZHABREN = generateFish("zhabren", 1, true), //
	        IO = generateFish("io", 1, true), //
	        CARPENDUM = generateFish("carpendum", 1, true), //
	        ZLATENICA = generateFish("zlatenica", 1, true), //
	        RED_RAG = generateFish("red_rag", 2, true), //
	        LAZURITE_SALMON = generateFish("lazurite_salmon", 2, true), //
	        LINARIUS = generateFish("linarius", 2, true), //
	        LITTLE_STRUSTIC = generateFish("little_strustic", 2, true), //
	        MALDENDU = generateFish("maldendu", 2, true), //
	        MILIDONIUS = generateFish("milidonius", 3, true), //
	        NARLUN = generateFish("narlun", 3, true), //
	        NEONMUS = generateFish("neonmus", 3, true), //
	        NIDLAN = generateFish("nidlan", 3, true), //
	        NIGHT_LADY = generateFish("night_lady", 3, true), //
	        PAVEYA = generateFish("paveya", 3, true), //
	        SHAZAN = generateFish("shazan", 3, true), //
	        SEMGA = generateFish("semga", 3, true);
	
	public static final TwoTuple<ItemSkilledFish, ItemAdvancementIcon[]> generateFish(String name, int lvl, boolean advancements)
	{
		ItemAdvancementIcon[] p2 = new ItemAdvancementIcon[advancements ? 5 : 0];
		ItemSkilledFish fish = new ItemSkilledFish(name, lvl, 8 + lvl);
		
		SimpleRegistration.registerItem(fish, InfoMV.MOD_ID, MVStuff.tab);
		
		if(advancements)
		{
			p2[0] = new ItemAdvancementIcon(name + "_1");
			p2[1] = new ItemAdvancementIcon(name + "_100");
			p2[2] = new ItemAdvancementIcon(name + "_250");
			p2[3] = new ItemAdvancementIcon(name + "_500");
			p2[4] = new ItemAdvancementIcon(name + "_999");
			for(ItemAdvancementIcon ai : p2)
				SimpleRegistration.registerItem(ai, InfoMV.MOD_ID, MVStuff.tab);
		}
		
		return new TwoTuple<>(fish, p2);
	}
}