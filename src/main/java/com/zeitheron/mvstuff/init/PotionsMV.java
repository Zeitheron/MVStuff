package com.zeitheron.mvstuff.init;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.zeitheron.mvstuff.potions.PotionDamageDeflector;
import com.zeitheron.mvstuff.potions.PotionHarshHunger;
import com.zeitheron.mvstuff.potions.PotionLesserNausea;
import com.zeitheron.mvstuff.potions.PotionLesserSlowness;

import net.minecraft.potion.Potion;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class PotionsMV
{
	public static final PotionDamageDeflector DAMAGE_DEFLECTOR = new PotionDamageDeflector();
	public static final PotionLesserSlowness LESSER_SLOWNESS = new PotionLesserSlowness();
	public static final PotionLesserNausea LESSER_NAUSEA = new PotionLesserNausea();
	public static final PotionHarshHunger HARSH_HUNGER = new PotionHarshHunger();
	
	public static void register()
	{
		IForgeRegistry<Potion> reg = GameRegistry.findRegistry(Potion.class);
		
		for(Field f : PotionsMV.class.getDeclaredFields())
		{
			f.setAccessible(true);
			if(Potion.class.isAssignableFrom(f.getType()) && Modifier.isStatic(f.getModifiers()))
				try
				{
					reg.register((Potion) f.get(null));
				} catch(IllegalArgumentException | IllegalAccessException e)
				{
					e.printStackTrace();
				}
		}
	}
}