package com.zeitheron.mvstuff.net;

import java.util.UUID;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketOtherPlayerUseItem implements IPacket
{
	static
	{
		IPacket.handle(PacketOtherPlayerUseItem.class, PacketOtherPlayerUseItem::new);
	}
	
	public static void sendToAllExceptExecutor(EntityPlayer player, EnumHand hand)
	{
		if(player instanceof EntityPlayerMP)
		{
			PacketOtherPlayerUseItem packet = new PacketOtherPlayerUseItem();
			packet.hand = hand;
			packet.uuid = player.getGameProfile().getId();
			for(EntityPlayerMP mp : player.getServer().getPlayerList().getPlayers())
				if(!mp.getUniqueID().equals(player.getUniqueID()))
					HCNet.INSTANCE.sendTo(packet, mp);
		}
		
	}
	
	public EnumHand hand;
	public UUID uuid;
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setBoolean("Main", hand == EnumHand.MAIN_HAND);
		nbt.setUniqueId("UUID", uuid);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		hand = nbt.getBoolean("Main") ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND;
		uuid = nbt.getUniqueId("UUID");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		try
		{
			EntityPlayer player = Minecraft.getMinecraft().world.getPlayerEntityByUUID(uuid);
			if(player != null)
				player.getHeldItem(hand).getItem().onItemRightClick(player.world, player, hand);
		} catch(Throwable err)
		{
		}
		return null;
	}
}