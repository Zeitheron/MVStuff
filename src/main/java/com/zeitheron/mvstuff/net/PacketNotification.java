package com.zeitheron.mvstuff.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.UTF8Strings;
import com.zeitheron.mvstuff.client.otr.OTRSurprise;

import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketNotification implements IPacket
{
	static
	{
		IPacket.handle(PacketNotification.class, PacketNotification::new);
	}
	
	public ITextComponent[] lines;
	
	public PacketNotification(ITextComponent... lines)
	{
		this.lines = lines;
	}
	
	public PacketNotification()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		NBTTagList list = new NBTTagList();
		for(ITextComponent c : lines)
			list.appendTag(new NBTTagByteArray(UTF8Strings.utf8Bytes(ITextComponent.Serializer.componentToJson(c))));
		nbt.setTag("Lines", list);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		NBTTagList list = nbt.getTagList("Lines", NBT.TAG_BYTE_ARRAY);
		lines = new ITextComponent[list.tagCount()];
		for(int i = 0; i < lines.length; ++i)
			lines[i] = ITextComponent.Serializer.jsonToComponent(UTF8Strings.utf8Str(((NBTTagByteArray) list.get(i)).getByteArray()));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		OTRSurprise.show(lines);
		return null;
	}
}