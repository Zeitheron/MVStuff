package com.zeitheron.mvstuff.net;

import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.mvstuff.init.GuiCallbacksMV;

import net.minecraft.entity.player.EntityPlayerMP;

@MainThreaded
public class PacketOpenBelt implements IPacket
{
	static
	{
		IPacket.handle(PacketOpenBelt.class, PacketOpenBelt::new);
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		EntityPlayerMP mp = net.getSender();
		GuiManager.openGuiCallback(GuiCallbacksMV.BELT_CALLBACK, mp, new WorldLocation(mp.world, mp.getPosition()));
		return null;
	}
}