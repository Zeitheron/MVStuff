package com.zeitheron.mvstuff.net;

import java.util.UUID;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.mvstuff.client.rendering.PlayerItemUseRendering;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;

@MainThreaded
public class PacketApplyScroll implements IPacket
{
	static
	{
		IPacket.handle(PacketApplyScroll.class, PacketApplyScroll::new);
	}
	
	public UUID uid;
	public ItemStack stack;
	
	public static void sendToAllExceptExecutor(EntityPlayer player, ItemStack stack)
	{
		if(player instanceof EntityPlayerMP)
			HCNet.INSTANCE.sendToDimension(new PacketApplyScroll(player.getGameProfile().getId(), stack), player.world.provider.getDimension());
	}
	
	public PacketApplyScroll(UUID uid, ItemStack stack)
	{
		this.stack = stack;
		this.uid = uid;
	}
	
	public PacketApplyScroll()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("id", uid);
		nbt.setTag("Item", stack.serializeNBT());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		uid = nbt.getUniqueId("id");
		stack = new ItemStack(nbt.getCompoundTag("Item"));
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		PlayerItemUseRendering.apply(uid.toString().toLowerCase(), stack);
		return null;
	}
}