package com.zeitheron.mvstuff.proxy;

import org.lwjgl.input.Keyboard;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.client.otr.OTRManager;
import com.zeitheron.mvstuff.client.rendering.PlayerItemUseRendering;
import com.zeitheron.mvstuff.entity.EntityOldMonkProjectile;
import com.zeitheron.mvstuff.entity.EntityOverlordProjectile;
import com.zeitheron.mvstuff.entity.EntityZeitheronProjectile;
import com.zeitheron.mvstuff.init.ItemsMV;
import com.zeitheron.mvstuff.net.PacketOpenBelt;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class ClientProxy extends ServerProxy
{
	public static final KeyBinding openStorageBelt = new KeyBinding("key." + InfoMV.MOD_ID + ":openStorageBelt", Keyboard.KEY_L, "key.categories.inventory");
	
	@Override
	public void preInit()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityZeitheronProjectile.class, mgr -> new RenderSnowball<>(mgr, ItemsMV.PROJECTILE_ZEITHERON, Minecraft.getMinecraft().getRenderItem()));
		RenderingRegistry.registerEntityRenderingHandler(EntityOverlordProjectile.class, mgr -> new RenderSnowball<>(mgr, ItemsMV.PROJECTILE_OVERLORD, Minecraft.getMinecraft().getRenderItem()));
		RenderingRegistry.registerEntityRenderingHandler(EntityOldMonkProjectile.class, mgr -> new RenderSnowball<>(mgr, ItemsMV.PROJECTILE_OLD_MONK, Minecraft.getMinecraft().getRenderItem()));
		MinecraftForge.EVENT_BUS.register(new PlayerItemUseRendering());
	}
	
	@Override
	public void init()
	{
		ClientRegistry.registerKeyBinding(openStorageBelt);
		openStorageBelt.setKeyConflictContext(KeyConflictContext.IN_GAME);
	}
	
	public boolean osbPress;
	
	@SubscribeEvent
	public void clientTick(ClientTickEvent e)
	{
		if(e.phase != Phase.END)
			return;
		OTRManager.INSTANCE.update();
		boolean osb = openStorageBelt.isKeyDown();
		if(osbPress != osb)
		{
			osbPress = osb;
			if(osb)
				HCNet.INSTANCE.sendToServer(new PacketOpenBelt());
		}
	}
	
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void renderScreen(RenderGameOverlayEvent e)
	{
		if(e.getType() == ElementType.ALL)
		{
			float pa = e.getPartialTicks();
			
			OTRManager.INSTANCE.render(pa);
		}
	}
	
	@Override
	public void addNausea(EntityPlayer player, float amount, float cap)
	{
		if(player instanceof EntityPlayerSP)
		{
			EntityPlayerSP sp = (EntityPlayerSP) player;
			sp.timeInPortal = Math.min(cap, sp.timeInPortal + amount);
		}
	}
	
	@Override
	public void resetNausea(EntityPlayer player)
	{
		if(player instanceof EntityPlayerSP)
		{
			EntityPlayerSP sp = (EntityPlayerSP) player;
			sp.timeInPortal = 0;
			sp.prevTimeInPortal = 0;
		}
	}
}