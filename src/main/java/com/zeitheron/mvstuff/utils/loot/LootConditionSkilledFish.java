package com.zeitheron.mvstuff.utils.loot;

import java.util.Random;

import com.zeitheron.mvstuff.events.PlayerManagerMV;
import com.zeitheron.mvstuff.events.PlayerManagerMV.PlayerDataMV;
import com.zeitheron.mvstuff.items.ItemSkilledFish;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.conditions.LootCondition;

public class LootConditionSkilledFish implements LootCondition
{
	public final ItemSkilledFish fish;
	
	public LootConditionSkilledFish(ItemSkilledFish fish)
	{
		this.fish = fish;
	}
	
	@Override
	public boolean testCondition(Random rand, LootContext context)
	{
		if(fish.chance.apply(rand) != Boolean.TRUE)
			return false;
		Entity ent = context.getKillerPlayer();
		if(ent instanceof EntityPlayerMP)
		{
			PlayerDataMV pd = PlayerManagerMV.getData((EntityPlayerMP) ent);
			return pd != null && pd.getCurrentFishingLvl() >= fish.lvl;
		}
		return false;
	}
}