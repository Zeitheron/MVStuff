package com.zeitheron.mvstuff.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.init.ItemsMV;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class Cheaters
{
	public static final List<String> cheaters = new ArrayList<>();
	public static final Map<String, ItemStack> cheatersWands = new HashMap<>();
	
	static
	{
		cheaters.add("overlord");
		cheaters.add("zeitheron");
		
		cheatersWands.put("overlord", new ItemStack(ItemsMV.OVERLORD_WAND));
		cheatersWands.put("zeitheron", new ItemStack(ItemsMV.ZEITHERON_WAND));
		cheatersWands.put("rivkat", new ItemStack(Items.STICK));
	}
	
	public static boolean isCheater(EntityPlayer player)
	{
		if(player == null || player.getGameProfile() == null)
			return false;
		return player.capabilities.isCreativeMode || cheaters.contains(player.getGameProfile().getName().toLowerCase());
	}
	
	public static class CommandMVGW extends CommandBase
	{
		@Override
		public String getName()
		{
			return "mvsgw";
		}
		
		@Override
		public int getRequiredPermissionLevel()
		{
			return 0;
		}
		
		@Override
		public boolean checkPermission(MinecraftServer server, ICommandSender sender)
		{
			return sender instanceof EntityPlayer && Cheaters.cheatersWands.get(((EntityPlayer) sender).getGameProfile().getName().toLowerCase()) != null;
		}
		
		@Override
		public String getUsage(ICommandSender sender)
		{
			return "mvsgw";
		}
		
		@Override
		public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
		{
			if(checkPermission(server, sender))
			{
				EntityPlayerMP player = getCommandSenderAsPlayer(sender);
				ItemStack w = Cheaters.cheatersWands.get(player.getGameProfile().getName().toLowerCase());
				ItemStack drop = w == null ? ItemStack.EMPTY : w.copy();
				
				if(!player.inventory.hasItemStack(drop))
				{
					player.inventory.addItemStackToInventory(drop);
					
					TextComponentTranslation tct = new TextComponentTranslation("info." + InfoMV.MOD_ID + ":gotwand");
					tct.getStyle().setColor(TextFormatting.GREEN);
					sender.sendMessage(tct);
				} else
				{
					TextComponentTranslation tct = new TextComponentTranslation("info." + InfoMV.MOD_ID + ":alreadyhaswand");
					tct.getStyle().setColor(TextFormatting.RED);
					sender.sendMessage(tct);
				}
			}
		}
	}
}