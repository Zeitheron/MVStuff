package com.zeitheron.mvstuff.utils;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.net.PacketNotification;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.DisplayInfo;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class Advancements
{
	public static void complete(EntityPlayerMP mp, String type, boolean notify)
	{
		if(mp != null && !mp.world.isRemote && mp.getServer() != null)
		{
			Advancement adv = mp.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(InfoMV.MOD_ID, "main/" + type));
			if(adv == null)
				return;
			AdvancementProgress ap = mp.getAdvancements().getProgress(adv);
			if(ap != null && !ap.isDone())
			{
				for(String criteria : ap.getRemaningCriteria())
					ap.grantCriterion(criteria);
				
				if(notify)
				{
					DisplayInfo di = adv.getDisplay();
					ITextComponent title = di.getTitle().createCopy();
					title.getStyle().setColor(TextFormatting.GREEN);
					title = new TextComponentTranslation("info." + InfoMV.MOD_ID + ":advcomplete").appendSibling(title);
					HCNet.INSTANCE.sendTo(new PacketNotification(title, di.getDescription()), mp);
				}
			}
		}
	}
}