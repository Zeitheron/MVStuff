package com.zeitheron.mvstuff.commands;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.lib.zlib.utils.Joiner;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.events.PlayerManagerMV;
import com.zeitheron.mvstuff.events.PlayerManagerMV.PlayerDataMV;
import com.zeitheron.mvstuff.items.ItemSkilledFish;
import com.zeitheron.mvstuff.net.PacketNotification;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.server.command.CommandTreeBase;

public class CommandMV extends CommandTreeBase
{
	{
		addSubcommand(new CommandBase()
		{
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "";
			}
			
			@Override
			public String getName()
			{
				return "fishing";
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args[0].equalsIgnoreCase("reset"))
				{
					EntityPlayerMP mp = args.length >= 2 ? getPlayer(server, sender, args[1]) : getCommandSenderAsPlayer(sender);
					
					PlayerDataMV pd = PlayerManagerMV.getData(mp);
					pd.fishesCaught.clear();
					pd.totalFishCount = 0;
					pd.fishingLevel = 0;
					pd.fishingLevelCurrent = 0;
					
					for(ItemSkilledFish isf : ItemSkilledFish.FISHES)
					{
						ResourceLocation[] loc = new ResourceLocation[5];
						loc[0] = new ResourceLocation(InfoMV.MOD_ID, "main/adv" + isf.getRegistryName().getPath().substring(4) + "_1");
						loc[1] = new ResourceLocation(InfoMV.MOD_ID, "main/adv" + isf.getRegistryName().getPath().substring(4) + "_100");
						loc[2] = new ResourceLocation(InfoMV.MOD_ID, "main/adv" + isf.getRegistryName().getPath().substring(4) + "_250");
						loc[3] = new ResourceLocation(InfoMV.MOD_ID, "main/adv" + isf.getRegistryName().getPath().substring(4) + "_500");
						loc[4] = new ResourceLocation(InfoMV.MOD_ID, "main/adv" + isf.getRegistryName().getPath().substring(4) + "_999");
						for(ResourceLocation rl : loc)
						{
							Advancement a = server.getAdvancementManager().getAdvancement(rl);
							if(a != null)
							{
								PlayerAdvancements pa = mp.getAdvancements();
								AdvancementProgress ap = pa.getProgress(a);
								for(String criteria : ap.getCompletedCriteria())
									pa.revokeCriterion(a, criteria);
							}
						}
					}
					
					if(mp != sender)
					{
						mp.sendMessage(new TextComponentTranslation("info." + InfoMV.MOD_ID + ":fishingreset.fother", sender.getName()));
						mp.sendMessage(new TextComponentTranslation("info." + InfoMV.MOD_ID + ":fishingreset.tother", mp.getName()));
					} else
						mp.sendMessage(new TextComponentTranslation("info." + InfoMV.MOD_ID + ":fishingreset.self"));
				}
			}
			
			@Override
			public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos)
			{
				if(args.length == 1)
					return getListOfStringsMatchingLastWord(args, "reset");
				if(args.length == 2)
					return getListOfStringsMatchingLastWord(args, server.getPlayerList().getOnlinePlayerNames());
				return Collections.emptyList();
			}
		});
		
		addSubcommand(new CommandBase()
		{
			@Override
			public String getUsage(ICommandSender sender)
			{
				return null;
			}
			
			@Override
			public String getName()
			{
				return "notification";
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				EntityPlayerMP mp = args.length >= 1 ? getPlayer(server, sender, args[0]) : getCommandSenderAsPlayer(sender);
				if(mp != null)
				{
					String[] lns = Joiner.on(" ").join(Arrays.asList(Arrays.copyOfRange(args, 1, args.length))).split("<br>");
					ITextComponent[] lntc = new ITextComponent[lns.length];
					for(int i = 0; i < lns.length; ++i)
						lntc[i] = new TextComponentTranslation(lns[i].trim().replaceAll("@username@", mp.getGameProfile().getName()).replaceAll("@time@", new SimpleDateFormat("hh:mm:ss").format(Date.from(Instant.now()))));
					HCNet.INSTANCE.sendTo(new PacketNotification(lntc), mp);
				}
			}
			
			@Override
			public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos)
			{
				if(args.length == 1)
					return getListOfStringsMatchingLastWord(args, server.getPlayerList().getOnlinePlayerNames());
				return Collections.emptyList();
			}
		});
		
		addSubcommand(new CommandBase()
		{
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "commands.give.usage";
			}
			
			@Override
			public String getName()
			{
				return "give";
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args.length < 2)
				{
					throw new WrongUsageException("commands.give.usage", new Object[0]);
				} else
				{
					EntityPlayer entityplayer = getPlayer(server, sender, args[0]);
					Item item = getItemByText(sender, args[1]);
					int i = args.length >= 3 ? parseInt(args[2], 1, item.getItemStackLimit()) : 1;
					int j = args.length >= 4 ? parseInt(args[3]) : 0;
					ItemStack itemstack = new ItemStack(item, i, j);
					
					if(args.length >= 5)
					{
						String s = buildString(args, 4);
						
						try
						{
							itemstack.setTagCompound(JsonToNBT.getTagFromJson(s));
						} catch(NBTException nbtexception)
						{
							throw new CommandException("commands.give.tagError", new Object[] { nbtexception.getMessage() });
						}
					}
					
					boolean flag = entityplayer.inventory.addItemStackToInventory(itemstack);
					
					if(flag)
					{
						entityplayer.world.playSound((EntityPlayer) null, entityplayer.posX, entityplayer.posY, entityplayer.posZ, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.PLAYERS, 0.2F, ((entityplayer.getRNG().nextFloat() - entityplayer.getRNG().nextFloat()) * 0.7F + 1.0F) * 2.0F);
						entityplayer.inventoryContainer.detectAndSendChanges();
					}
					
					if(flag && itemstack.isEmpty())
					{
						itemstack.setCount(1);
						sender.setCommandStat(CommandResultStats.Type.AFFECTED_ITEMS, i);
						EntityItem entityitem1 = entityplayer.dropItem(itemstack, false);
						
						if(entityitem1 != null)
						{
							entityitem1.makeFakeItem();
						}
					} else
					{
						sender.setCommandStat(CommandResultStats.Type.AFFECTED_ITEMS, i - itemstack.getCount());
						EntityItem entityitem = entityplayer.dropItem(itemstack, false);
						
						if(entityitem != null)
						{
							entityitem.setNoPickupDelay();
							entityitem.setOwner(entityplayer.getName());
						}
					}
					
					sender.sendMessage(new TextComponentTranslation("commands.give.success", itemstack.getTextComponent(), i, entityplayer.getName()));
				}
			}
			
			@Override
			public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
			{
				if(args.length == 1)
					return getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
				else
					return args.length == 2 ? getListOfStringsMatchingLastWord(args, Item.REGISTRY.getKeys()) : Collections.emptyList();
			}
			
			@Override
			public boolean isUsernameIndex(String[] args, int index)
			{
				return index == 0;
			}
		});
		
		addSubcommand(new CommandBase()
		{
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "set variable";
			}
			
			@Override
			public String getName()
			{
				return "setvar";
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				EntityPlayerMP mp = getPlayer(server, sender, args[0]);
				if(args[1].equalsIgnoreCase("total_fish"))
				{
					PlayerDataMV pd = PlayerManagerMV.getData(mp);
					pd.totalFishCount = parseInt(args[2], 0);
				} else if(args[1].equalsIgnoreCase("specific_fish"))
				{
					ItemSkilledFish fish = WorldUtil.cast(ForgeRegistries.ITEMS.getValue(new ResourceLocation(args[2])), ItemSkilledFish.class);
					if(fish != null)
					{
						PlayerDataMV pd = PlayerManagerMV.getData(mp);
						Integer i = pd.fishesCaught.remove(fish);
						if(i == null)
							i = 0;
						pd.totalFishCount -= i.intValue();
						pd.addFish(fish, parseInt(args[3], 0));
					} else
						throw new CommandException("Fish " + args[2] + " not found!");
				}
			}
			
			@Override
			public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos)
			{
				if(args.length == 1)
					return getListOfStringsMatchingLastWord(args, server.getPlayerList().getOnlinePlayerNames());
				if(args.length == 2)
					return getListOfStringsMatchingLastWord(args, "total_fish", "specific_fish");
				if(args.length == 3)
					if(args[1].equalsIgnoreCase("specific_fish"))
						return getListOfStringsMatchingLastWord(args, ItemSkilledFish.FISHES.stream().map(i -> i.getRegistryName().toString()).collect(Collectors.toList()));
				return super.getTabCompletions(server, sender, args, targetPos);
			}
		});
	}
	
	@Override
	public String getName()
	{
		return "mvstuff";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "Access to various admin functions";
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return super.checkPermission(server, sender) || (sender instanceof EntityPlayerMP && ((EntityPlayerMP) sender).getGameProfile().getName().equalsIgnoreCase("zeitheron"));
	}
}