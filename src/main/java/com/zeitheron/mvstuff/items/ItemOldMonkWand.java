package com.zeitheron.mvstuff.items;

import com.google.common.collect.Multimap;
import com.zeitheron.mvstuff.entity.EntityOldMonkProjectile;
import com.zeitheron.mvstuff.net.PacketOtherPlayerUseItem;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemOldMonkWand extends Item
{
	public ItemOldMonkWand()
	{
		setTranslationKey("old_monk_wand");
		setMaxStackSize(1);
		setMaxDamage(250);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		playerIn.getCooldownTracker().setCooldown(this, 30);
		playerIn.getHeldItem(handIn).damageItem(1, playerIn);
		EntityOldMonkProjectile ent = new EntityOldMonkProjectile(worldIn, playerIn);
		ent.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0F, 1.5F, 0F);
		worldIn.spawnEntity(ent);
		playerIn.addStat(StatList.getObjectUseStats(this));
		if(!worldIn.isRemote)
			PacketOtherPlayerUseItem.sendToAllExceptExecutor(playerIn, handIn);
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
	{
		if(entityIn.ticksExisted % (20 * 60) == 0 && stack.getItemDamage() > 0)
			stack.setItemDamage(stack.getItemDamage() - 1);
	}
	
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
	{
		stack.damageItem(1, attacker);
		target.addPotionEffect(new PotionEffect(MobEffects.SLOWNESS, 20 * 10, 1));
		return super.hitEntity(stack, target, attacker);
	}
	
	@Override
	public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot)
	{
		Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);
		
		if(equipmentSlot == EntityEquipmentSlot.MAINHAND)
		{
			multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", 11D, 0));
			multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", -2.4000000953674316D, 0));
		}
		
		return multimap;
	}
}