package com.zeitheron.mvstuff.items;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemUnariusNegators extends Item implements IBauble
{
	public ItemUnariusNegators()
	{
		setTranslationKey("unarius_negators");
	}
	
	@Override
	public BaubleType getBaubleType(ItemStack itemstack)
	{
		return BaubleType.TRINKET;
	}
}