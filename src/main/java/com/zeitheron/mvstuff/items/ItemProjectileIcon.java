package com.zeitheron.mvstuff.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemProjectileIcon extends Item
{
	public ItemProjectileIcon(String name)
	{
		setTranslationKey("projectile_" + name);
		setMaxStackSize(1);
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
	}
}