package com.zeitheron.mvstuff.items;

import java.text.DecimalFormat;
import java.util.List;

import com.zeitheron.mvstuff.InfoMV;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCooldownable extends Item
{
	protected int cd;
	final DecimalFormat TimeConqueror = new DecimalFormat("#0.0");
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn)
	{
		EntityPlayer player = Minecraft.getMinecraft().player;
		if(player != null)
		{
			float cd = player.getCooldownTracker().getCooldown(this, 1);
			
			if(cd > 0.001F)
				tooltip.add(I18n.format("info." + InfoMV.MOD_ID + ":cooldown", TimeConqueror.format(cd * this.cd)));
		}
	}
}