package com.zeitheron.mvstuff.items;

import java.util.List;

import com.zeitheron.mvstuff.MVStuff;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemAdvancementIcon extends Item
{
	public ItemAdvancementIcon(String name)
	{
		setTranslationKey("adv_" + name);
		setMaxStackSize(1);
	}
	
	@Override
	public Item setCreativeTab(CreativeTabs tab)
	{
		return super.setCreativeTab(MVStuff.tabAdvancements);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(I18n.format(getTranslationKey() + ".desc"));
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
		super.getSubItems(tab, items);
	}
}