package com.zeitheron.mvstuff.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;

public class ItemSkilledFish extends ItemFood implements IRegisterListener
{
	public static final List<ItemSkilledFish> FISHES = new ArrayList<>();
	
	public float lvl;
	public Function<Random, Boolean> chance;
	
	public ItemSkilledFish(String name, float fishingLevel, int oneInN)
	{
		this(name, fishingLevel, rand -> rand.nextInt(oneInN) == 0);
	}
	
	public ItemSkilledFish(String name, float fishingLevel, Function<Random, Boolean> chance)
	{
		super(3 + Math.round(fishingLevel * 2), (fishingLevel + 1) * 2, true);
		setTranslationKey("fish_" + name);
		this.lvl = fishingLevel;
		this.chance = chance;
	}
	
	@Override
	public void onRegistered()
	{
		FISHES.add(this);
	}
}