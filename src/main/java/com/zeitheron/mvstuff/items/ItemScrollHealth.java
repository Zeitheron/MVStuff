package com.zeitheron.mvstuff.items;

import com.zeitheron.mvstuff.net.PacketApplyScroll;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemScrollHealth extends ItemCooldownable
{
	final int cooldown;
	final float heal;
	
	public ItemScrollHealth(float heal, int cooldown)
	{
		this.heal = heal;
		this.cooldown = cooldown;
		setTranslationKey("scroll_health");
		setMaxStackSize(16);
		this.cd = cooldown;
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		PacketApplyScroll.sendToAllExceptExecutor(playerIn, playerIn.getHeldItem(handIn));
		playerIn.getHeldItem(handIn).shrink(1);
		playerIn.heal(heal);
		playerIn.getCooldownTracker().setCooldown(this, cooldown * 20);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}