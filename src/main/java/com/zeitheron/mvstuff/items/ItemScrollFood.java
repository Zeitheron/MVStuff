package com.zeitheron.mvstuff.items;

import com.zeitheron.mvstuff.net.PacketApplyScroll;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemScrollFood extends Item
{
	final boolean full;
	
	public ItemScrollFood(boolean full)
	{
		this.full = full;
		setTranslationKey("scroll_saturation" + (full ? "_full" : ""));
		setMaxStackSize(16);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		PacketApplyScroll.sendToAllExceptExecutor(playerIn, playerIn.getHeldItem(handIn));
		playerIn.getHeldItem(handIn).shrink(1);
		playerIn.getFoodStats().addStats(full ? 20 : 8, full ? 5 : 1);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}