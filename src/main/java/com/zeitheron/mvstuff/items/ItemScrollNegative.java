package com.zeitheron.mvstuff.items;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.mvstuff.init.PotionsMV;
import com.zeitheron.mvstuff.net.PacketApplyScroll;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemScrollNegative extends ItemCooldownable implements IRegisterListener
{
	static final Set<ItemScrollNegative> scrolls = new HashSet<>();
	
	final int cooldown, timeInSec;
	
	public ItemScrollNegative(int cooldown, int timeInSec, int tier)
	{
		this.cooldown = cooldown;
		this.timeInSec = timeInSec;
		setTranslationKey("scroll_zdmg_" + tier);
		setMaxStackSize(16);
		this.cd = cooldown;
	}
	
	@Override
	public void onRegistered()
	{
		scrolls.add(this);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		PacketApplyScroll.sendToAllExceptExecutor(playerIn, playerIn.getHeldItem(handIn));
		playerIn.getHeldItem(handIn).shrink(1);
		playerIn.addPotionEffect(new PotionEffect(PotionsMV.DAMAGE_DEFLECTOR, timeInSec * 20));
		scrolls.forEach(scroll -> playerIn.getCooldownTracker().setCooldown(scroll, cooldown * 20));
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}