package com.zeitheron.mvstuff.inventory.slot;

import com.zeitheron.mvstuff.items.ItemStorageBelt;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotNotTakeableBelt extends Slot
{
	public SlotNotTakeableBelt(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		if(stack.getItem() instanceof ItemStorageBelt)
			return false;
		return true;
	}
	
	@Override
	public boolean canTakeStack(EntityPlayer playerIn)
	{
		if(!getStack().isEmpty() && (getStack().getItem() instanceof ItemStorageBelt))
			return false;
		return true;
	}
}