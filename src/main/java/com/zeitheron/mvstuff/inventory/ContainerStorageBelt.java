package com.zeitheron.mvstuff.inventory;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import com.zeitheron.mvstuff.inventory.slot.SlotNotTakeableBelt;
import com.zeitheron.mvstuff.items.ItemStorageBelt;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.Constants.NBT;

public class ContainerStorageBelt extends TransferableContainer<ItemStack>
{
	public InventoryDummy inv;
	
	public ContainerStorageBelt(EntityPlayer player, ItemStack t)
	{
		super(player, t, 8, 82);
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		if(!playerIn.world.isRemote)
			if(!t.isEmpty())
			{
				if(!t.hasTagCompound())
					t.setTagCompound(new NBTTagCompound());
				NBTTagCompound tag = new NBTTagCompound();
				inv.writeToNBT(tag);
				t.getTagCompound().setTag("Items", tag);
			}
		super.onContainerClosed(playerIn);
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return t.getItem() instanceof ItemStorageBelt;
	}
	
	@Override
	protected void addCustomSlots()
	{
		inv = new InventoryDummy(9 * ((ItemStorageBelt) t.getItem()).rows);
		if(!t.isEmpty())
		{
			if(t.hasTagCompound() && t.getTagCompound().hasKey("Items", NBT.TAG_COMPOUND))
				inv.readFromNBT(t.getTagCompound().getCompoundTag("Items"));
			for(int j = 0; j < inv.getSizeInventory() / 9; ++j)
				for(int k = 0; k < 9; ++k)
					addSlotToContainer(new SlotNotTakeableBelt(inv, k + j * 9, 8 + k * 18, 18 + j * 18));
		}
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);
		
		if(slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			
			if(index < inv.getSizeInventory())
			{
				if(!this.mergeItemStack(itemstack1, inv.getSizeInventory(), this.inventorySlots.size(), false))
					return ItemStack.EMPTY;
			} else if(!this.mergeItemStack(itemstack1, 0, inv.getSizeInventory(), false))
				return ItemStack.EMPTY;
			
			if(itemstack1.isEmpty())
				slot.putStack(ItemStack.EMPTY);
			else
				slot.onSlotChanged();
		}
		
		return itemstack;
	}
}