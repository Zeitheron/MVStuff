package com.zeitheron.mvstuff.inventory;

import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.mvstuff.InfoMV;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerHorseInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiStorageBelt extends GuiWTFMojang
{
	public final ResourceLocation texture = new ResourceLocation(InfoMV.MOD_ID, "textures/gui/storage_belt.png");
	
	ItemStack beltStack;
	
	public GuiStorageBelt(EntityPlayer player, ItemStack belt)
	{
		super(new ContainerStorageBelt(player, belt));
		beltStack = belt;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		String text = beltStack.getDisplayName();
		fontRenderer.drawString(text, (xSize - fontRenderer.getStringWidth(text)) / 2, 6, 0x444444, false);
		fontRenderer.drawString(((ContainerStorageBelt) inventorySlots).player.inventory.getDisplayName().getFormattedText(), 8, 72, 0x444444, false);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		UtilsFX.bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			drawTexturedModalRect(guiLeft + s.xPos - 1, guiTop + s.yPos - 1, xSize, 0, 18, 18);
	}
}