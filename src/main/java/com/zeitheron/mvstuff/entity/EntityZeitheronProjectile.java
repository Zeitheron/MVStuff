package com.zeitheron.mvstuff.entity;

import java.util.Objects;
import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityZeitheronProjectile extends EntityThrowable
{
	public EntityZeitheronProjectile(World worldIn)
	{
		super(worldIn);
	}
	
	public EntityZeitheronProjectile(World worldIn, EntityLivingBase throwerIn)
	{
		super(worldIn, throwerIn);
	}
	
	public EntityZeitheronProjectile(World worldIn, double x, double y, double z)
	{
		super(worldIn, x, y, z);
	}
	
	@SideOnly(Side.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		// Called on collide
		if(id == 3)
		{
			
		}
	}
	
	@Override
	protected float getGravityVelocity()
	{
		return 0.03F;
	}
	
	@Override
	protected void onImpact(RayTraceResult result)
	{
		dmg: if(result.entityHit != null)
		{
			UUID target = result.entityHit.getUniqueID();
			UUID throwe = getThrower() != null ? getThrower().getUniqueID() : null;
			
			if(Objects.equals(target, throwe))
				break dmg;
			
			result.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, getThrower()), 50F);
			if(!this.world.isRemote && result.entityHit instanceof EntityLivingBase)
				((EntityLivingBase) result.entityHit).addPotionEffect(new PotionEffect(MobEffects.WITHER, 20 * 40, 5));
			if(getThrower() != null)
				getThrower().heal(2F);
		}
		
		if(!this.world.isRemote)
			this.world.setEntityState(this, (byte) 3);
		
		this.setDead();
	}
}