package com.zeitheron.mvstuff.entity;

import java.util.Objects;
import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityOldMonkProjectile extends EntityThrowable
{
	public EntityOldMonkProjectile(World worldIn)
	{
		super(worldIn);
	}
	
	public EntityOldMonkProjectile(World worldIn, EntityLivingBase throwerIn)
	{
		super(worldIn, throwerIn);
	}
	
	public EntityOldMonkProjectile(World worldIn, double x, double y, double z)
	{
		super(worldIn, x, y, z);
	}
	
	@SideOnly(Side.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		// Called on collide
		if(id == 3)
		{
			
		}
	}
	
	@Override
	protected float getGravityVelocity()
	{
		return 0.03F;
	}
	
	@Override
	protected void onImpact(RayTraceResult result)
	{
		dmg: if(result.entityHit != null)
		{
			UUID target = result.entityHit.getUniqueID();
			UUID throwe = getThrower() != null ? getThrower().getUniqueID() : null;
			
			if(Objects.equals(target, throwe))
				break dmg;
			
			result.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, getThrower()), 12F);
			if(!this.world.isRemote && result.entityHit instanceof EntityLivingBase)
				((EntityLivingBase) result.entityHit).addPotionEffect(new PotionEffect(MobEffects.SLOWNESS, 20 * 10, 1));
		}
		
		if(!this.world.isRemote)
			this.world.setEntityState(this, (byte) 3);
		
		this.setDead();
	}
}