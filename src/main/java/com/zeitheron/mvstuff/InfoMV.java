package com.zeitheron.mvstuff;

public class InfoMV
{
	public static final String //
	MOD_ID = "mvstuff", //
	        MOD_NAME = "Minevostorg Stuff", //
	        VERSION = "@VERSION@";
	
	public static final String //
	PROXY_BASE = "com.zeitheron.mvstuff.proxy.", //
	        CLIENT_PROXY = PROXY_BASE + "ClientProxy", //
	        SERVER_PROXY = PROXY_BASE + "ServerProxy";
}