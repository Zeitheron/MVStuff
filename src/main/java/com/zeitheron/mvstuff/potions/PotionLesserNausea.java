package com.zeitheron.mvstuff.potions;

import java.lang.reflect.Field;

import com.zeitheron.mvstuff.MVStuff;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.attributes.AbstractAttributeMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;

public class PotionLesserNausea extends PotionBase
{
	public PotionLesserNausea()
	{
		super(true, 0x44AA44, "lesser_nausea");
	}
	
	@Override
	public boolean isReady(int duration, int amplifier)
	{
		return true;
	}
	
	@Override
	public void tickPotion(EntityLivingBase entityLivingBaseIn, int amplifier)
	{
		if(entityLivingBaseIn instanceof EntityPlayer)
		{
			PotionEffect our = entityLivingBaseIn.getActivePotionEffect(this);
			PotionEffect nausea;
			if((nausea = entityLivingBaseIn.getActivePotionEffect(MobEffects.NAUSEA)) == null)
				entityLivingBaseIn.addPotionEffect(new PotionEffect(MobEffects.NAUSEA, our.getDuration(), 0, true, false));
			else if(Math.abs(nausea.getDuration() - our.getDuration()) > 1)
			{
				Field duration = PotionEffect.class.getDeclaredFields()[2];
				duration.setAccessible(true);
				try
				{
					duration.setInt(nausea, our.getDuration());
				} catch(Throwable err)
				{
				}
			}
			MVStuff.proxy.addNausea((EntityPlayer) entityLivingBaseIn, 0, amplifier / 255F);
		}
	}
	
	@Override
	public void removeAttributesModifiersFromEntity(EntityLivingBase entityLivingBaseIn, AbstractAttributeMap attributeMapIn, int amplifier)
	{
		entityLivingBaseIn.removePotionEffect(MobEffects.NAUSEA);
		if(entityLivingBaseIn instanceof EntityPlayer)
			MVStuff.proxy.resetNausea((EntityPlayer) entityLivingBaseIn);
	}
}