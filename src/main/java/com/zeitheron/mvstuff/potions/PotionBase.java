package com.zeitheron.mvstuff.potions;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.utils.Cheaters;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.attributes.AbstractAttributeMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PotionBase extends Potion
{
	public PotionBase(boolean bad, int color, String name)
	{
		super(bad, color);
		setPotionName("potion." + InfoMV.MOD_ID + ":" + name);
		setRegistryName(InfoMV.MOD_ID, name);
	}
	
	public String getTex()
	{
		return "textures/potions/" + getRegistryName().getPath() + ".png";
	}
	
	public void tickPotion(EntityLivingBase entity, int amplifier)
	{
		
	}
	
	@Override
	public final void performEffect(EntityLivingBase entityLivingBaseIn, int amplifier)
	{
		if(isBadEffect() && entityLivingBaseIn instanceof EntityPlayer && Cheaters.isCheater((EntityPlayer) entityLivingBaseIn))
			return;
		tickPotion(entityLivingBaseIn, amplifier);
	}
	
	@Override
	public void applyAttributesModifiersToEntity(EntityLivingBase entityLivingBaseIn, AbstractAttributeMap attributeMapIn, int amplifier)
	{
		if(isBadEffect() && entityLivingBaseIn instanceof EntityPlayer && Cheaters.isCheater((EntityPlayer) entityLivingBaseIn))
			return;
		super.applyAttributesModifiersToEntity(entityLivingBaseIn, attributeMapIn, amplifier);
	}
	
	@Override
	public boolean isReady(int duration, int amplifier)
	{
		return true;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void renderHUDEffect(int x, int y, PotionEffect effect, Minecraft mc, float alpha)
	{
		UtilsFX.bindTexture(InfoMV.MOD_ID, getTex());
		GlStateManager.enableBlend();
		RenderUtil.drawFullTexturedModalRect(x + 3, y + 3, 18, 18);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void renderInventoryEffect(int x, int y, PotionEffect effect, Minecraft mc)
	{
		UtilsFX.bindTexture(InfoMV.MOD_ID, getTex());
		GlStateManager.enableBlend();
		RenderUtil.drawFullTexturedModalRect(x + 6, y + 6, 18, 18);
	}
}