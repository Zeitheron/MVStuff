package com.zeitheron.mvstuff.potions;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.item.ItemStack;

public class PotionLesserSlowness extends PotionBase
{
	public PotionLesserSlowness()
	{
		super(true, 0x444444, "lesser_slowness");
		registerPotionAttributeModifier(SharedMonsterAttributes.MOVEMENT_SPEED, "648D7064-EA90-4F59-BABE-C2C23A6DD7A9", 0.0D, 2);
	}
	
	@Override
	public double getAttributeModifierAmount(int amplifier, AttributeModifier modifier)
	{
		return -1 * (amplifier / 255F);
	}
	
	@Override
	public List<ItemStack> getCurativeItems()
	{
		return new ArrayList<>();
	}
}