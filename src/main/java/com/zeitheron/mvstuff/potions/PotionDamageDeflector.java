package com.zeitheron.mvstuff.potions;

import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PotionDamageDeflector extends PotionBase
{
	public PotionDamageDeflector()
	{
		super(false, 0xFFFF00, "damage_deflector");
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void entityAttacked(LivingHurtEvent e)
	{
		PotionEffect pe = e.getEntityLiving().getActivePotionEffect(this);
		if(pe != null)
			e.setCanceled(true);
	}
}