package com.zeitheron.mvstuff.potions;

import java.lang.reflect.Field;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.FoodStats;

public class PotionHarshHunger extends PotionBase
{
	public PotionHarshHunger()
	{
		super(true, 0x963C00, "harsh_hunger");
	}
	
	@Override
	public boolean isReady(int duration, int amplifier)
	{
		// Activate once per 3 seconds
		return duration % 60 == 0;
	}
	
	@Override
	public void tickPotion(EntityLivingBase entityLivingBaseIn, int amplifier)
	{
		if(entityLivingBaseIn instanceof EntityPlayer)
		{
			FoodStats fs = ((EntityPlayer) entityLivingBaseIn).getFoodStats();
			fs.setFoodLevel(2);
			
			try
			{
				Field foodSaturationLevel = FoodStats.class.getDeclaredFields()[1];
				foodSaturationLevel.setAccessible(true);
				foodSaturationLevel.setFloat(fs, 2F);
			} catch(IllegalArgumentException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}
	}
}