package com.zeitheron.mvstuff.client.rendering;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.zeitheron.hammercore.client.utils.TexturePixelGetter;
import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.mvstuff.client.particle.FXSparkle;
import com.zeitheron.mvstuff.client.particle.FXWisp;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class PlayerItemUseRendering
{
	public static void init()
	{
	}
	
	static
	{
		MinecraftForge.EVENT_BUS.register(new PlayerItemUseRendering());
	}
	
	public static final IndexedMap<String, List<SCRNDRInfo>> infos = new IndexedMap<>();
	
	public static void apply(String player, ItemStack scroll)
	{
		List<SCRNDRInfo> infl = infos.get(player);
		if(infl == null)
			infos.put(player, infl = new ArrayList<>());
		infl.add(new SCRNDRInfo(scroll));
	}
	
	public void render(EntityPlayer player, List<SCRNDRInfo> sis, double x, double y, double z, float pt)
	{
		if(sis != null)
			for(SCRNDRInfo si : sis)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x, y, z);
				float time = si.ticks + pt;
				
				GlStateManager.rotate(si.degreeOffset + time * 6F, 0, 1, 0);
				GlStateManager.translate(2 * (1F - Math.max(0, time - 100) / 100F), -1 + Math.min(100, time) / 100 * 2.125F, 0);
				GlStateManager.rotate(90, 0, 1, 0);
				
				Minecraft.getMinecraft().getRenderItem().renderItem(si.stack, TransformType.GROUND);
				
				GlStateManager.popMatrix();
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(x, y, z);
				
				GlStateManager.rotate(si.degreeOffset + time * 6F + 180, 0, 1, 0);
				GlStateManager.translate(2 * (1F - Math.max(0, time - 100) / 100F), -1 + Math.min(100, time) / 100 * 2.125F, 0);
				GlStateManager.rotate(90, 0, 1, 0);
				
				Minecraft.getMinecraft().getRenderItem().renderItem(si.stack, TransformType.GROUND);
				
				GlStateManager.popMatrix();
			}
	}
	
	@SubscribeEvent
	public void renderPlayer(RenderPlayerEvent.Post e)
	{
		EntityPlayer player = e.getEntityPlayer();
		// No not render self player here
		if(Minecraft.getMinecraft().player != player)
			render(player, infos.get(player.getGameProfile().getId().toString().toLowerCase()), e.getX(), e.getY(), e.getZ(), e.getPartialRenderTick());
	}
	
	@SubscribeEvent
	public void renderFirstPerson(RenderWorldLastEvent e)
	{
		EntityPlayer player = Minecraft.getMinecraft().player;
		render(player, infos.get(player.getGameProfile().getId().toString().toLowerCase()), 0, 0, 0, e.getPartialTicks());
	}
	
	@SubscribeEvent
	public void clientTick(ClientTickEvent e)
	{
		if(e.phase != Phase.END || Minecraft.getMinecraft().isGamePaused())
			return;
		List<List<SCRNDRInfo>> infov = infos.getValues();
		for(int i = 0; i < infov.size(); ++i)
		{
			String id = infos.getKey(i);
			
			List<SCRNDRInfo> sis = infov.get(i);
			if(id != null)
				for(int j = 0; j < sis.size(); ++j)
				{
					SCRNDRInfo si = sis.get(j);
					
					si.ticks++;
					EntityPlayer ep = Minecraft.getMinecraft().world.getPlayerEntityByUUID(UUID.fromString(id));
					if(ep != null)
					{
						float mh = -1 + Math.min(100, si.ticks) / 100F * 2.125F;
						float off = 2 * (1F - Math.max(0, si.ticks - 100) / 100F);
						
						ParticleProxy_Client.queueParticleSpawn(new FXWisp(Minecraft.getMinecraft().world, ep.posX + Math.sin(Math.toRadians(si.degreeOffset + 120 + si.ticks * 6)) * off, ep.posY + .2F + mh, ep.posZ + Math.cos(Math.toRadians(si.degreeOffset + 120 + si.ticks * 6)) * off, .8F).setColor(si.colors[ep.getRNG().nextInt(si.colors.length)]).setGravity(.03F));
						ParticleProxy_Client.queueParticleSpawn(new FXWisp(Minecraft.getMinecraft().world, ep.posX + Math.sin(Math.toRadians(si.degreeOffset + 290 + si.ticks * 6)) * off, ep.posY + .2F + mh, ep.posZ + Math.cos(Math.toRadians(si.degreeOffset + 290 + si.ticks * 6)) * off, .8F).setColor(si.colors[ep.getRNG().nextInt(si.colors.length)]).setGravity(.03F));
						
						ParticleProxy_Client.queueParticleSpawn(new FXSparkle(ep.world, ep.posX + Math.cos(Math.toRadians(si.degreeOffset + 120 + si.ticks * 6)) * (.5 + .75 * off), ep.posY + 2.2525F, ep.posZ + Math.sin(Math.toRadians(si.degreeOffset + 120 + si.ticks * 6)) * (.5 + .75 * off), si.colors[ep.getRNG().nextInt(si.colors.length)], 36).setGravity(.03F));
						ParticleProxy_Client.queueParticleSpawn(new FXSparkle(ep.world, ep.posX + Math.cos(Math.toRadians(si.degreeOffset + 290 + si.ticks * 6)) * (.5 + .75 * off), ep.posY + 2.2525F, ep.posZ + Math.sin(Math.toRadians(si.degreeOffset + 290 + si.ticks * 6)) * (.5 + .75 * off), si.colors[ep.getRNG().nextInt(si.colors.length)], 36).setGravity(.03F));
					}
					if(si.ticks >= 200)
					{
						sis.remove(j);
						--j;
					}
				}
		}
	}
	
	public static class SCRNDRInfo
	{
		public final ItemStack stack;
		public int[] colors;
		public int ticks;
		public float degreeOffset;
		
		public SCRNDRInfo(ItemStack stack)
		{
			this.stack = stack;
			colors = TexturePixelGetter.getAllColors(stack);
			degreeOffset = new Random().nextFloat() * 360F;
		}
	}
}