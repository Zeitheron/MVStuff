package com.zeitheron.mvstuff.client.otr;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.zeitheron.hammercore.client.utils.RenderUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.ITextComponent;

public class OTRSurprise extends OTR
{
	private static List<OTRSurprise> queue = new ArrayList<>();
	private static OTRSurprise current;
	
	public static void show(ITextComponent... lines)
	{
		Minecraft.getMinecraft().addScheduledTask(() ->
		{
			if(current != null)
				queue.add(new OTRSurprise(lines));
			else
				OTRManager.add(new OTRSurprise(lines));
		});
	}
	
	public final String[] lines;
	public final int[] widths;
	public final int maxWidth;
	
	public OTRSurprise(ITextComponent... lines)
	{
		this.lines = new String[lines.length];
		this.widths = new int[lines.length];
		
		int mw = 0;
		for(int i = 0; i < lines.length; ++i)
		{
			this.lines[i] = lines[i].getFormattedText();
			mw = Math.max(mw, this.widths[i] = Minecraft.getMinecraft().fontRenderer.getStringWidth(this.lines[i]));
		}
		
		this.maxWidth = mw;
		
		expiresAfter = 100 + 40;
	}
	
	public int scale;
	final Random rand = new Random();
	
	@Override
	public void update()
	{
		if(scale < 20)
		{
			++scale;
			
			float scale = 2 * toSine(Math.min(20F, this.scale) / 20F * decay(20));
			
			FontRenderer font = Minecraft.getMinecraft().fontRenderer;
			
			int esc = font.FONT_HEIGHT + 2;
			int totalWidth = Math.min(width, maxWidth);
			int totalHeight = lines.length * esc;
			
			float w = totalWidth * scale, h = totalHeight * scale;
			float x = (width - w) / 2F, y = (height + totalHeight * 2 - totalHeight * scale) / 2F;
			
			int particles = 5 + rand.nextInt(12);
			for(int i = 0; i < particles; ++i)
			{
				boolean left = rand.nextBoolean();
				
				float px = (left ? x : x + w) / width, py = (y + h * rand.nextFloat()) / height;
				
				OTRManager.add(new OTRSparkle(px, py, .3F * rand.nextFloat(), (left ? 5F : -5F) * rand.nextFloat() * 3F, 8F * (rand.nextFloat() - rand.nextFloat() * .5F), 60 + rand.nextInt(60)).withColor(rand));
			}
		} else
		{
			float scale = 2 * toSine(Math.min(20F, this.scale) / 20F * decay(20));
			
			FontRenderer font = Minecraft.getMinecraft().fontRenderer;
			
			int esc = font.FONT_HEIGHT + 2;
			int totalWidth = Math.min(width, maxWidth);
			int totalHeight = lines.length * esc;
			
			float w = totalWidth * scale, h = totalHeight * scale;
			float x = (width - w) / 2F, y = (height + totalHeight * 2 - totalHeight * scale) / 2F;
			
			int particles = 2 + rand.nextInt(3);
			
			for(int i = 0; i < particles; ++i)
			{
				boolean left = rand.nextBoolean();
				
				float px = (4 + (left ? x + (w - 12) - (w - 12) * rand.nextFloat() : x + (w - 12) * rand.nextFloat())) / width, py = (y + h * rand.nextFloat()) / height;
				
				OTRManager.add(new OTRSparkle(px, py, .1F * rand.nextFloat(), 0, 0, 65 + rand.nextInt(65)).withColor(rand));
			}
		}
		
		current = this;
		
		super.update();
		
		if(expired)
		{
			current = null;
			if(!queue.isEmpty())
			{
				current = queue.remove(0);
				OTRManager.add(current);
			}
		}
	}
	
	@Override
	public void render(float partialTicks)
	{
		float d = decay(20);
		float scale = 2 * toSine(Math.min(20F, this.scale + partialTicks) / 20F * d);
		
		FontRenderer font = Minecraft.getMinecraft().fontRenderer;
		
		int esc = font.FONT_HEIGHT + 2;
		int totalWidth = Math.min(width, maxWidth);
		int totalHeight = lines.length * esc;
		
		GlStateManager.pushMatrix();
		GlStateManager.translate((width - totalWidth * scale) / 2F, (height + totalHeight * 2 - totalHeight * scale) / 2F, 100);
		
		int alpha = (int) (.2F * d * 255);
		
		RenderUtil.drawGradientRect(0, 0, totalWidth * scale, totalHeight * scale, alpha << 24 | 0x000000, alpha << 24 | 0x000000, -2);
		alpha = (int) (d * 255);
		
		GlStateManager.enableBlend();
		
		GlStateManager.scale(scale, scale, 1);
		int lni = 0;
		for(String ln : lines)
		{
			if(widths[lni] > width)
			{
				float scaleFactor = width / (float) widths[lni];
				GlStateManager.pushMatrix();
				GlStateManager.scale(scaleFactor, scaleFactor, 1F);
				font.drawString(ln, 0, lni * esc, alpha << 24 | 0xFFFFFF, true);
				GlStateManager.popMatrix();
			} else
				font.drawString(ln, (totalWidth - widths[lni]) / 2F, 1 + lni * esc, alpha << 24 | 0xFFFFFF, true);
			++lni;
		}
		GlStateManager.popMatrix();
	}
}