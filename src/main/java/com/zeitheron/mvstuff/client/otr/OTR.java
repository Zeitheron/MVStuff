package com.zeitheron.mvstuff.client.otr;

import net.minecraft.util.math.MathHelper;

public class OTR
{
	public int width, height;
	
	public int ticksExisted;
	public boolean expired;
	protected int expiresAfter = 0;
	{
		if(this instanceof IExpirable)
			expiresAfter = ((IExpirable) this).expiresAfter();
	}
	
	public void onResize(int oldWidth, int oldHeight, int newWidth, int newHeight)
	{
		this.width = newWidth;
		this.height = newHeight;
	}
	
	public void setDimension(int width, int height)
	{
		if(this.width != width || this.height != height)
			onResize(this.width, this.height, width, height);
	}
	
	public void update()
	{
		ticksExisted++;
		if(expiresAfter > 0 && ticksExisted > expiresAfter)
			expired = true;
	}
	
	public int phase(int max)
	{
		float te = ticksExisted + partialTime;
		if(expiresAfter > 0)
			return Math.round(te / (float) expiresAfter * max);
		return 0;
	}
	
	protected float partialTime = 1F;
	
	public float toSine(float val)
	{
		return MathHelper.sin((float) Math.toRadians(val * 90F));
	}
	
	public float decay(int ticks)
	{
		float te = ticksExisted + partialTime;
		if(expiresAfter > 0 && expiresAfter - te <= ticks && ticks > 0F)
			return Math.max(toSine((expiresAfter - te) / (float) ticks), 0);
		return 1F;
	}
	
	public void render(float partialTicks)
	{
	}
}