package com.zeitheron.mvstuff.client.otr;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

public class OTRManager
{
	public static final OTRManager INSTANCE = new OTRManager();
	
	public static void add(OTR effect)
	{
		INSTANCE.effects.add(effect);
	}
	
	public final List<OTR> effects = new ArrayList<>();
	
	public void update()
	{
		if(Minecraft.getMinecraft().isGamePaused())
			return;
		
		ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
		
		int total = effects.size();
		for(int i = 0; i < total; ++i)
		{
			OTR e = effects.get(i);
			e.setDimension(sr.getScaledWidth(), sr.getScaledHeight());
			e.update();
			if(e.expired)
			{
				effects.remove(i);
				total = effects.size();
				--i;
			}
		}
	}
	
	public void render(float pt)
	{
		int total = effects.size();
		for(int i = 0; i < total; ++i)
		{
			OTR e = effects.get(i);
			ColorHelper.gl(0xFF_FFFFFF);
			e.partialTime = pt;
			e.render(pt);
		}
	}
}