package com.zeitheron.mvstuff.client.otr;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.mvstuff.InfoMV;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class OTRSparkle extends OTR
{
	public float x, y, grav, mx, my;
	public int rgba;
	
	public OTRSparkle(float x, float y, float gravity, float motionX, float motionY, int ticks)
	{
		this.expiresAfter = ticks;
		this.x = x;
		this.y = y;
		this.grav = gravity;
		this.mx = motionX;
		this.my = motionY;
	}
	
	public OTRSparkle withColor(Random rand)
	{
		this.rgba = 255 << 24 | rand.nextInt(255) << 16 | rand.nextInt(255) << 8 | rand.nextInt(255);
		return this;
	}
	
	public OTRSparkle withColor(int rgba)
	{
		this.rgba = rgba;
		return this;
	}
	
	@Override
	public void update()
	{
		my -= grav;
		
		x -= mx / width;
		y -= my / height;
		
		mx *= .788887F;
		my *= .788887F;
		
		super.update();
	}
	
	@Override
	public void render(float partialTicks)
	{
		UtilsFX.bindTexture(InfoMV.MOD_ID, "textures/particles/sparkle.png");
		
		float nx = x - mx / width;
		float ny = y - my / height;
		
		double cx = (x + (nx - x) * partialTime) * width;
		double cy = (y + (ny - y) * partialTime) * height;
		
		float scale = 1 / 12F;
		
		if(ticksExisted < 5)
			scale *= ticksExisted / 5F;
		
		if(ticksExisted >= expiresAfter - 5)
			scale *= 1 - (ticksExisted - expiresAfter + 5) / 5F;
		
		GlStateManager.enableAlpha();
		GL11.glEnable(GL11.GL_BLEND);
		RenderHelper.disableStandardItemLighting();
		
		GL11.glColor4f(ColorHelper.getRed(rgba), ColorHelper.getGreen(rgba), ColorHelper.getBlue(rgba), .9F * ColorHelper.getAlpha(rgba));
		
		for(int i = 0; i < 3; ++i)
		{
			float ps = i == 0 ? scale : i == 2 ? (float) ((Math.sin(hashCode() % 90 + ticksExisted / 2) + 1) / 2.5 * scale) : scale / 2;
			
			GL11.glPushMatrix();
			GL11.glBlendFunc(770, i == 0 ? 771 : 772);
			GL11.glTranslated(cx - 64 * ps / 2, cy - 64 * ps / 2, 5);
			GL11.glScaled(ps, ps, ps);
			RenderUtil.drawTexturedModalRect(0, 0, phase(2) * 64, 0, 64, 64);
			GL11.glPopMatrix();
		}
		
		GL11.glBlendFunc(770, 771);
		GL11.glColor4f(1, 1, 1, 1);
	}
}