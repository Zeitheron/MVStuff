package com.zeitheron.mvstuff.client.otr;

public interface IExpirable
{
	int expiresAfter();
}