package com.zeitheron.mvstuff.api;

import net.minecraft.item.ItemStack;

public interface IBait
{
	static float getBaitPowerF(ItemStack item)
	{
		if(!item.isEmpty() && item.getItem() instanceof IBait)
			return ((IBait) item.getItem()).getBaitPower(item);
		return 0F;
	}
	
	float getBaitPower(ItemStack stack);
}