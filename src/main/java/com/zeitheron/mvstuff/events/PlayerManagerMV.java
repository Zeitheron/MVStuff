package com.zeitheron.mvstuff.events;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mvstuff.InfoMV;
import com.zeitheron.mvstuff.MVStuff;
import com.zeitheron.mvstuff.client.rendering.PlayerItemUseRendering;
import com.zeitheron.mvstuff.items.ItemSkilledFish;
import com.zeitheron.mvstuff.utils.Advancements;
import com.zeitheron.mvstuff.utils.Cheaters;

import it.unimi.dsi.fastutil.ints.Int2IntArrayMap;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.ItemFishedEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

@MCFBus
public class PlayerManagerMV
{
	public static final Map<String, NonNullList<ItemStack>> retainedDrops = new HashMap<>();
	public static final Map<String, PlayerDataMV> playerDatas = new HashMap<>();
	
	private static final List<String> loggingOut = new ArrayList<>();
	
	public static PlayerDataMV getData(EntityPlayerMP player)
	{
		return playerDatas.get(player.getGameProfile().getName().toLowerCase());
	}
	
	@SubscribeEvent
	public void playerLoggedOut(PlayerLoggedOutEvent e)
	{
		loggingOut.add(e.player.getGameProfile().getName().toLowerCase());
	}
	
	@SubscribeEvent
	public void savePlayerData(PlayerEvent.SaveToFile e)
	{
		String un = e.getEntityPlayer().getGameProfile().getName().toLowerCase();
		File f = new File(e.getPlayerDirectory(), e.getPlayerUUID() + "-mvstuff.dat");
		try(FileOutputStream fos = new FileOutputStream(f))
		{
			NBTTagCompound nbt = new NBTTagCompound();
			
			NBTTagList retain = new NBTTagList();
			{
				NonNullList<ItemStack> l = retainedDrops.get(un);
				if(l != null)
					for(ItemStack d : l)
						retain.appendTag(d.serializeNBT());
			}
			nbt.setTag("RetainedItems", retain);
			PlayerDataMV pd = playerDatas.get(un);
			if(pd != null)
				nbt.setTag("Data", pd.serializeNBT());
			CompressedStreamTools.writeCompressed(nbt, fos);
			if(loggingOut.remove(un))
			{
				retainedDrops.remove(un);
				playerDatas.remove(un);
			}
		} catch(Throwable err)
		{
			MVStuff.LOG.fatal("FAILED TO SAVE %s's DATA: %s", err);
		}
	}
	
	@SubscribeEvent
	public void loadPlayerData(PlayerEvent.LoadFromFile e)
	{
		EntityPlayer player = e.getEntityPlayer();
		
		if(player instanceof EntityPlayerMP && !player.world.isRemote && player.getServer() != null)
		{
			EntityPlayerMP mp = (EntityPlayerMP) player;
			MinecraftServer mc = player.getServer();
			Advancement adv = mc.getAdvancementManager().getAdvancement(new ResourceLocation(InfoMV.MOD_ID, "main/root"));
			if(adv == null)
				return;
			AdvancementProgress ap = mp.getAdvancements().getProgress(adv);
			if(!ap.isDone())
				for(String criteria : ap.getRemaningCriteria())
					ap.grantCriterion(criteria);
		}
		
		File f = new File(e.getPlayerDirectory(), e.getPlayerUUID() + "-mvstuff.dat");
		if(f.isFile())
			try(FileInputStream fis = new FileInputStream(f))
			{
				NBTTagCompound nbt = CompressedStreamTools.readCompressed(fis);
				
				String un = e.getEntityPlayer().getGameProfile().getName().toLowerCase();
				
				NBTTagList retain = nbt.getTagList("RetainedItems", NBT.TAG_COMPOUND);
				{
					NonNullList<ItemStack> l = NonNullList.create();
					for(int i = 0; i < retain.tagCount(); ++i)
						l.add(new ItemStack(retain.getCompoundTagAt(i)));
					retainedDrops.put(un, l);
				}
				
				PlayerDataMV pd = new PlayerDataMV(e.getEntityPlayer());
				pd.deserializeNBT(nbt.getCompoundTag("Data"));
				playerDatas.put(un, pd);
			} catch(Throwable err)
			{
			}
	}
	
	@SubscribeEvent
	public void playerRespawn(PlayerRespawnEvent e)
	{
		if(e.player instanceof EntityPlayer)
		{
			NonNullList<ItemStack> l = retainedDrops.remove(e.player.getGameProfile().getName().toLowerCase());
			if(l != null)
				l.forEach(e.player.inventory::addItemStackToInventory);
		}
	}
	
	@SubscribeEvent
	public void playerDrops(LivingDropsEvent e)
	{
		if(e.getEntityLiving() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) e.getEntityLiving();
			String un = player.getGameProfile().getName().toLowerCase();
			NonNullList<ItemStack> retained = NonNullList.create();
			if(Cheaters.isCheater(player))
				for(EntityItem item : e.getDrops())
				{
					boolean retain = false;
					
					if(item.getItem().isItemEqual(Cheaters.cheatersWands.get(un)) || item.getItem().getItem().getRegistryName().getNamespace().equalsIgnoreCase(InfoMV.MOD_ID))
						retain = true;
					
					if(retain)
					{
						retained.add(item.getItem().copy());
						item.setDead();
					}
				}
			if(!retained.isEmpty())
				retainedDrops.put(un, retained);
		}
	}
	
	@SubscribeEvent
	public void itemFished(ItemFishedEvent e)
	{
		PlayerDataMV pd = getData(WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class));
		if(pd != null)
			for(ItemStack item : e.getDrops())
				if(item.getItem() instanceof ItemSkilledFish)
					pd.addFish((ItemSkilledFish) item.getItem(), item.getCount());
		PlayerItemUseRendering.init();
	}
	
	public static class PlayerDataMV implements INBTSerializable<NBTTagCompound>
	{
		public static final IntList GLOBAL_FISH_CATCH = new IntArrayList();
		public static final Int2IntMap FISH_TO_LVLS = new Int2IntArrayMap();
		static
		{
			GLOBAL_FISH_CATCH.add(1);
			GLOBAL_FISH_CATCH.add(10);
			GLOBAL_FISH_CATCH.add(50);
			GLOBAL_FISH_CATCH.add(100);
			GLOBAL_FISH_CATCH.add(500);
			
			FISH_TO_LVLS.put(31, 1);
			FISH_TO_LVLS.put(61, 2);
			FISH_TO_LVLS.put(91, 3);
			FISH_TO_LVLS.put(121, 4);
			FISH_TO_LVLS.put(200, 5);
		}
		
		public final UUID playerUUID;
		
		public PlayerDataMV(EntityPlayer player)
		{
			playerUUID = player.getGameProfile().getId();
		}
		
		@Nullable
		public EntityPlayerMP getPlayerMP()
		{
			MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if(server != null)
				return server.getPlayerList().getPlayerByUUID(playerUUID);
			return null;
		}
		
		public int totalFishCount;
		
		public int fishingLevel;
		public float fishingLevelCurrent;
		
		public final Object2IntMap<ItemSkilledFish> fishesCaught = new Object2IntArrayMap<>();
		{
			fishesCaught.defaultReturnValue(0);
		}
		
		public float getCurrentFishingLvl()
		{
			return fishingLevel + fishingLevelCurrent;
		}
		
		public void addFish(ItemSkilledFish fish, int amt)
		{
			totalFishCount += amt;
			fishesCaught.put(fish, fishesCaught.getInt(fish) + 1);
			
			EntityPlayerMP mp = getPlayerMP();
			
			if(GLOBAL_FISH_CATCH.contains(totalFishCount))
				Advancements.complete(mp, "adv_fish_" + totalFishCount, true);
			
			int lvl = fishingLevel;
			calculateFishingLvl();
			if(fishingLevel > lvl)
				for(int i = 1; i <= fishingLevel; ++i)
					Advancements.complete(mp, "adv_flvl_" + i, true);
				
			int caught = fishesCaught.getInt(fish);
			boolean advance = false;
			
			if(caught == 1 || caught == 100)
			{
				ItemStack terrasteel = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("botania", "manaresource")), 1, 4);
				if(!mp.inventory.addItemStackToInventory(terrasteel))
					WorldUtil.spawnItemStack(mp.world, mp.posX, mp.posY, mp.posZ, terrasteel);
				advance = true;
			} else if(caught == 250 || caught == 500)
			{
				ItemStack terrasteel = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("botania", "manaresource")), 5, 4);
				if(!mp.inventory.addItemStackToInventory(terrasteel))
					WorldUtil.spawnItemStack(mp.world, mp.posX, mp.posY, mp.posZ, terrasteel);
				advance = true;
			} else if(caught == 999)
			{
				ItemStack terrasteel = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("botania", "storage")), 10, 1);
				if(!mp.inventory.addItemStackToInventory(terrasteel))
					WorldUtil.spawnItemStack(mp.world, mp.posX, mp.posY, mp.posZ, terrasteel);
				advance = true;
			}
			
			if(advance)
				Advancements.complete(mp, "adv" + fish.getRegistryName().getPath().substring(4) + "_" + caught, true);
		}
		
		public void calculateFishingLvl()
		{
			IntArrayList keys = new IntArrayList(FISH_TO_LVLS.keySet());
			keys.sort((a, b) -> a.intValue() - b.intValue());
			
			for(int i = 0; i < keys.size(); ++i)
			{
				int amt = keys.getInt(i);
				if(totalFishCount >= amt)
				{
					fishingLevel = FISH_TO_LVLS.get(amt);
					fishingLevelCurrent = i < keys.size() - 1 ? (totalFishCount - amt) / (float) (keys.getInt(i + 1) - amt) : 0F;
				}
			}
		}
		
		@Override
		public NBTTagCompound serializeNBT()
		{
			NBTTagCompound nbt = new NBTTagCompound();
			
			nbt.setInteger("FishCaught", totalFishCount);
			nbt.setInteger("FishingLevel", fishingLevel);
			nbt.setFloat("FishingLevelCurrent", fishingLevelCurrent);
			
			NBTTagList fishingData = new NBTTagList();
			for(ItemSkilledFish fish : fishesCaught.keySet())
			{
				int amt = fishesCaught.getInt(fish);
				NBTTagCompound tag = new NBTTagCompound();
				tag.setString("Id", fish.getRegistryName().toString());
				tag.setInteger("Am", amt);
				fishingData.appendTag(tag);
			}
			nbt.setTag("FishingData", fishingData);
			
			return nbt;
		}
		
		@Override
		public void deserializeNBT(NBTTagCompound nbt)
		{
			totalFishCount = nbt.getInteger("FishCaught");
			fishingLevel = nbt.getInteger("FishingLevel");
			fishingLevelCurrent = nbt.getFloat("FishingLevelCurrent");
			
			fishesCaught.clear();
			NBTTagList list = nbt.getTagList("FishingData", NBT.TAG_COMPOUND);
			for(int i = 0; i < list.tagCount(); ++i)
			{
				NBTTagCompound tag = list.getCompoundTagAt(i);
				ItemSkilledFish isf = WorldUtil.cast(ForgeRegistries.ITEMS.getValue(new ResourceLocation(tag.getString("Id"))), ItemSkilledFish.class);
				if(isf != null)
					fishesCaught.put(isf, tag.getInteger("Am"));
			}
		}
	}
}