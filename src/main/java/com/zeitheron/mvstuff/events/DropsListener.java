package com.zeitheron.mvstuff.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.mvstuff.init.ItemsMV;
import com.zeitheron.mvstuff.init.PotionsMV;
import com.zeitheron.mvstuff.utils.Cheaters;

import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemPickupEvent;

@MCFBus
public class DropsListener
{
	public static boolean isImmuneToDebuffs(EntityPlayer player)
	{
		return Cheaters.isCheater(player);
	}
	
	public static boolean isPlayerImmune(EntityPlayer player)
	{
		if(Cheaters.isCheater(player))
			return true;
		IBaublesItemHandler baubles = BaublesApi.getBaublesHandler(player);
		for(int i = 0; i < baubles.getSlots(); ++i)
		{
			ItemStack stack = baubles.getStackInSlot(i);
			if(!stack.isEmpty() && stack.getItem() == ItemsMV.UNARIUS_NEGATORS)
				return true;
		}
		return false;
	}
	
	public static void applyHunger(EntityPlayer player)
	{
		// Ignore immune players...
		if(isPlayerImmune(player))
			return;
		PotionEffect pe = new PotionEffect(PotionsMV.HARSH_HUNGER, 20 * 60 * 60, 0, false, false);
		pe.setCurativeItems(new ArrayList<>());
		player.addPotionEffect(pe);
	}
	
	public static void applySlowness(EntityPlayer player)
	{
		// Ignore immune players...
		if(isPlayerImmune(player))
			return;
		PotionEffect pe = new PotionEffect(PotionsMV.LESSER_SLOWNESS, 20 * 60 * 30, (int) (.2F * 255), true, true);
		pe.setCurativeItems(new ArrayList<>());
		player.addPotionEffect(pe);
	}
	
	public static void applyNausea(EntityPlayer player)
	{
		// Ignore immune players...
		if(isPlayerImmune(player))
			return;
		PotionEffect pe = new PotionEffect(PotionsMV.LESSER_NAUSEA, 20 * 60 * 5, 75, false, true);
		pe.setCurativeItems(new ArrayList<>());
		player.addPotionEffect(pe);
	}
	
	public static void applyDebuff(EntityPlayer player)
	{
		switch(player.getRNG().nextInt(3))
		{
		case 0:
			applyHunger(player);
		break;
		case 1:
			applyNausea(player);
		break;
		case 2:
			applySlowness(player);
		break;
		}
	}
	
	public static List<Item> cursedItems = new ArrayList<>();
	
	static
	{
		Item[] bugs = new Item[] { ItemsMV.BLOOD_BUG, ItemsMV.CYAN_BUG, ItemsMV.SILMARINE_BUG, ItemsMV.SWAMP_BUG, ItemsMV.VIONLITHIC_BUG };
		Item[] butterflies = new Item[] { ItemsMV.BUTTERFLY_FIERY_FENIX, ItemsMV.BUTTERFLY_LAZURE_SMOKER, ItemsMV.BUTTERFLY_OPAL_NIGHT, ItemsMV.BUTTERFLY_QUEEN_NIGHT, ItemsMV.BUTTERFLY_TENDERIA };
		
		cursedItems.addAll(Arrays.asList(bugs));
		cursedItems.addAll(Arrays.asList(butterflies));
	}
	
	@SubscribeEvent
	public void pickup(ItemPickupEvent e)
	{
		if(!e.getStack().isEmpty() && cursedItems.contains(e.getStack().getItem()))
		{
			if(!e.player.world.isRemote && e.player.getRNG().nextInt(100) == 0)
				applyDebuff(e.player);
		}
	}
	
	@SubscribeEvent
	public void blockDrops(BlockEvent.HarvestDropsEvent e)
	{
		EntityPlayer player = e.getHarvester();
		
		if(player == null)
			return;
		
		Random rng = player.getRNG();
		IBlockState state = e.getState();
		
		// Do the dirty stuff
		if(state.getBlock().getRegistryName().getPath().toLowerCase().contains("dirt") || state.getBlock() instanceof BlockGrass)
		{
			if(rng.nextInt(1000) < 25)
			{
				Item[] bfs = new Item[] { ItemsMV.BLOOD_BUG, ItemsMV.CYAN_BUG, ItemsMV.SILMARINE_BUG, ItemsMV.SWAMP_BUG, ItemsMV.VIONLITHIC_BUG };
				e.getDrops().add(new ItemStack(bfs[rng.nextInt(bfs.length)]));
			}
			if(rng.nextInt(100) == 0)
			{
				applyDebuff(player);
			}
		} else
		// Do the grassy stuff
		if(state.getBlock().getRegistryName().getPath().toLowerCase().contains("grass"))
		{
			if(rng.nextInt(1000) < 30)
			{
				Item[] bfs = new Item[] { ItemsMV.BUTTERFLY_FIERY_FENIX, ItemsMV.BUTTERFLY_LAZURE_SMOKER, ItemsMV.BUTTERFLY_OPAL_NIGHT, ItemsMV.BUTTERFLY_QUEEN_NIGHT, ItemsMV.BUTTERFLY_TENDERIA };
				e.getDrops().add(new ItemStack(bfs[rng.nextInt(bfs.length)]));
			}
			if(rng.nextInt(100) == 0)
			{
				applyDebuff(player);
			}
		}
	}
}